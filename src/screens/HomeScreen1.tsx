//ventana principal
import React, {useState} from 'react'
import { View, Text, Button, StyleSheet, SafeAreaView, Alert, TextInput, Image} from 'react-native'
import { useNavigation } from '@react-navigation/native'

//botones
const Separator = () => (
    <View style={styles.separator} />
);

const HomeScreen1 = () => {
    const navigation = useNavigation();
    const handlePress = () => console.log("Text pressed");
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    return (
        <SafeAreaView style={styles.container}>
          <Image style={styles.Logo} source={require('./img/condor.png')} />
        <View>
          <Text style={styles.princ} numberOfLines={1} onPress={handlePress}>CORREO ELECTRÓNICO</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={text => setEmail(text)}
            placeholder="Email"
            value={email}
          />
          <Text style={styles.princ} numberOfLines={1} onPress={handlePress}>CONTRASEÑA</Text>
          <TextInput
            style={styles.textInput}
            onChangeText={text => setPassword}
            value={password}
            placeholder="password"
          />
        </View>
        <Separator />
        <View>
          <View style={styles.fixToText}>
            <Button
              title="REGISTRARME"
              color= 'purple'
              onPress={() => navigation.navigate("Registro")}
            />
            <Button
              title="INICIO SESIÓN"
              color= 'purple'
              onPress={() => navigation.navigate("Balance")}
            />
          </View>
          
        </View>
      </SafeAreaView>
    )
}
export default HomeScreen1

//dimensiones logo condor
const styles1 = StyleSheet.create({
    container: {
      paddingTop: 40,
    },
    condorLogo: { 
      width: 300,
      height: 300,
    },
});

const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingTop: 50,
      alignItems: 'center',
      display: 'flex',
    },
    title: {
      textAlign: 'center',
      marginVertical: 8,
      fontWeight: 'bold',
      fontStyle: 'italic',
      fontSize: 20,

    },
    textInput: {
      height: 40,
      width: 300,
      margin: 12,
      borderWidth: 1,
      padding: 10,
    },
    princ: {
      fontSize: 20,
      fontWeight: "bold",
      color: 'purple',
      alignItems: 'center',
      textAlign: 'center',
    },
    fixToText: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    separator: {
      marginVertical: 8,
      borderBottomColor: '#737373',
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
    Logo: {
        width: 200,
        height: 200,
        alignItems: 'center'
    }
});

